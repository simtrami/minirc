import os

from pytest import raises

import config


def test_address_port_params():
    config_content = "address = 10.0.0.1\nport = 123\n"
    c = config.load_config_from_content(config_content)
    assert c['address'] == "10.0.0.1"
    assert c['port'] == 123


def test_default_address_port():
    c = config.Config()
    assert c['address'] == "0.0.0.0"
    assert c['port'] == 12345


def test_user_database():
    users = config.UserDB()
    admin_user = users.get("admin")
    assert admin_user is not None
    assert admin_user["admin"] == True
    assert admin_user["password"] == "admin"

    users.add_user("toto", "tititoto", False)
    toto_user = users.get("toto")
    assert toto_user is not None
    assert toto_user["admin"] == False
    assert toto_user["password"] == "tititoto"


def test_load_userdb():
    users = config.load_userdb_from_content("toto tititoto True")
    user = users.get("toto")
    assert user is not None


def test_load_userdb_from_file():
    f = open("userdb.test", 'w+')
    f.write("toto tototo False\n")
    f.write("root rootro True")
    f.close()
    users = config.load_userdb_from_file("userdb.test")
    os.remove("userdb.test")
    user = users.get('toto')
    assert user is not None
    assert user['login'] == "toto"
    assert user['password'] == "tototo"
    assert user['admin'] == False
    user = users.get('root')
    assert user is not None
    assert user['login'] == "root"
    assert user['password'] == "rootro"
    assert user['admin'] == True


def test_invalid_config_parameter():
    config_content = "address = 10.0.0.1\nport = 123\nunknownparam = 456"
    with raises(config.ConfigError):
        config.load_config_from_content(config_content)


def test_multiple_ports():
    config_content = "ports = 1234, 12345"
    with raises(config.ConfigError):
        config.load_config_from_content(config_content)
    config_content = "port = 1234, 12345"
    with raises(config.ConfigError):
        config.load_config_from_content(config_content)
